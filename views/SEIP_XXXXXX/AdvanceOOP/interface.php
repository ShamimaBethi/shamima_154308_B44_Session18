<?php

class Bird{
    public $name = "Just a Bird";
    public function info(){

        echo "I'm $this->name <br>" ;
        echo "I'm a bird<br>";
    }

}


interface canFly{
    public function fly();
}

interface canSwim{
    public function swim();
}



class Dove extends Bird implements canFly{
    public $name="Dove";
    public function fly()
    {
        echo "I can Fly... <br>";
    }
}



class Penguin extends Bird implements canSwim{
    public $name="Penguin";
    public function swim()
    {
        echo "I can Swim... <br>";
    }
}



class Duck extends Bird implements canFly,canSwim{
    public $name="Duck";
    public function fly()
    {
        echo "I can Fly... <br>";
    }
    public function swim()
    {
        echo "I can Swim... <br>";
    }
}



function describe($bird){

    $bird->info();

    if($bird instanceof canFly){
        $bird->fly();
    }
    if($bird instanceof canSwim){
        $bird->swim();
    }
}


describe(new Bird());
echo "<br>";
describe(new Dove());
echo "<br>";
describe(new Penguin());
echo "<br>";
describe(new Duck());
