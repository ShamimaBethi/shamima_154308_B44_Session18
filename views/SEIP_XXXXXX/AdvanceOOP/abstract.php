<?php

abstract class MyAbsClass{
    public $a,$b;

    abstract protected function forceMethod1();
    abstract protected function forceMethod2($price);

    public function doSomething(){

        echo "I'm doing something... <br>";
    }

}



class MySimpleClass extends MyAbsClass{
    private  $simpleProperty1;

    public function forceMethod1()
    {
        echo "I'm inside ". __METHOD__ . "<br>";
    }

    public function forceMethod2($price)
    {
        echo "I'm inside ". __METHOD__ . "<br>";
    }

    public function simpleMethod1(){
        echo $this->simpleProperty1;
    }
}







