<?php

trait BigHello {
   public function sayHello() {
      echo 'HELLO ';
   }
}

trait SmallHello {
   public function sayHello() {
      echo 'hello';
   }
}

class HelloWorld{
   use BigHello, SmallHello{
       BigHello::sayHello insteadof SmallHello;
       SmallHello::sayHello as tellHello;
   }

   public function sayHelloWorld(){
      $this->sayHello();
      echo "<br>";
      $this->tellHello();
   }
}

$obj = new HelloWorld();
$obj->sayHelloWorld();

///////////////////////////////////


trait Hello {
   public function sayHello() {
      echo 'Hello ';
   }
}

trait World {
   public function sayWorld() {
      echo 'World!';
   }
}

trait HelloWorldTrait {
   use Hello, World;
}

class MyHelloWorldClass {
   use HelloWorldTrait;
}

$o = new MyHelloWorldClass();
$o->sayHello();
$o->sayWorld();





